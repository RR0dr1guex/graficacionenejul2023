﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Demo3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Image imagen;
        DispatcherTimer timer;
        int x = -400;
        public MainWindow()
        {
            InitializeComponent();
            imagen = new Image();
            imagen.Source = new BitmapImage(new Uri(@"https://cdn5.dibujos.net/dibujos/pintados/201848/computadora-la-casa-la-habitacion-11504103.jpg"));

            imagen.Height = 100;
            imagen.Width = 100;
            Lienzo.Children.Add(imagen);
            TranslateTransform translate = new TranslateTransform();
            translate.X = -400;
            translate.Y = 0;
            imagen.RenderTransform = translate;

        }

        private void btnIniciar_Click(object sender, RoutedEventArgs e)
        {
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 0, 0, 10);
            timer.Tick += Timer_Tick;
            timer.Start();

        }

        private void Timer_Tick(object? sender, EventArgs e)
        {
            TranslateTransform animacion
                = new TranslateTransform();
            animacion.X = x;
            x += 4;
            animacion.Y = 0;
            imagen.RenderTransform = animacion;
            if (x > 400)
            {
                timer.Stop();
                x = -400;
            }
        }
    }
}
